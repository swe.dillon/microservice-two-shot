from django.db import models

# Create your models here.


class LocationVO(models.Model):
    closet_name = models.CharField(max_length=50)
    import_href = models.CharField(max_length=50, unique=True)


class Hat(models.Model):
    name = models.CharField(max_length=50, null=True)
    color = models.CharField(max_length=50, null=True)
    fabric = models.CharField(max_length=50, null=True)
    style_name = models.CharField(max_length=50, null=True)
    picture = models.URLField(null=True)
    location = models.ForeignKey(
        LocationVO,
        related_name="location",
        on_delete=models.CASCADE,
        null=True
    )
