from django.shortcuts import render
from .models import Hat, LocationVO
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from common.json import ModelEncoder
import json
from django.core import serializers


class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href",
        "closet_name",
    ]


class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "name",
        "id",
        "color",
        "fabric",
        "style_name",
        "location",
        "picture",
    ]
    encoders = {
        "location": LocationVOEncoder(),
    }


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "name",
        "id",
        "color",
        "fabric",
        "style_name",
        "location",
        "picture",
    ]
    encoders = {
        "location": LocationVOEncoder(),
    }


# Create your views here.


@require_http_methods(["GET", "POST"])
def api_list_hats(request, location_vo_pk=None):
    if request.method == "GET":
        # hats = Hat.objects.all()
        if location_vo_pk is not None:
            hats = Hat.objects.filter(import_href=location_vo_pk)
        else:
            hats = Hat.objects.all()
        # hats_data = serializers.serialize('python', hats)
        # hats_list = [item['fields'] for item in hats_data]
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            # href = content["location"]
            # href = f'/api/locations/{location_vo_pk}/'
            location = LocationVO.objects.get(import_href=content["location"])
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        hats = Hat.objects.create(**content)
        return JsonResponse(
            hats,
            encoder=HatDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_hat(request, id):
    if request.method == "GET":
        try:
            hat = Hat.objects.get(id=id)
            return JsonResponse(
                hat,
                encoder=HatDetailEncoder,
                safe=False
            )
        except Hat.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid hat id"},
                status=400,
            )
    elif request.method == "DELETE":
        try:
            count, _ = Hat.objects.filter(id=id).delete()
            return JsonResponse({"deleted": count > 0})
        except Hat.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid hat id"},
                status=400,
            )
    else:
        try:
            content = json.loads(request.body)
            hats = Hat.objects.get(id=id)
            Hat.objects.filter(id=id).update(**content)
            return JsonResponse(
                hats,
                encoder=HatDetailEncoder,
                safe=False,
            )
        except Hat.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid hat id"},
                status=400,
            )
