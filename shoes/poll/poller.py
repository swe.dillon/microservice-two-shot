import django
import os
import sys
import time
import json
import requests
sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shoes_project.settings")
django.setup()
from api.shoes_rest.models import Shoe

def poll():
    while True:
        print('Shoes poller polling for data')
        try:
            response = requests.get('http://localhost:8080/bins')
            bins = response.json()['bins']
            for bin in bins:
                if Shoe.objects.filter(bin=bin['id']).exists():
                    continue
                shoe = Shoe(
                    manufacturer=bin['manufacturer'],
                    model_name=bin['model_name'],
                    color=bin['color'],
                    picture_url=bin['picture_url'],
                    bin=bin['id']
                )
                shoe.save()
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
