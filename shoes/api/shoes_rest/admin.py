from django.contrib import admin
from .models import Shoe, Bin

admin.site.register(Shoe)
admin.site.register(Bin)
