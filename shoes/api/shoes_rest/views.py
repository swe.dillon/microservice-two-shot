from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from json import loads
from .models import Shoe

@csrf_exempt
def shoe_view(request, shoe_id=None):
    if request.method == 'GET':
        shoes = Shoe.objects.all()
        shoe_list = []
        for shoe in shoes:
            shoe_dict = {
                'manufacturer': shoe.manufacturer,
                'model_name': shoe.model_name,
                'color': shoe.color,
                'picture_url': shoe.picture_url,
            }
            if shoe.bin is not None:
                shoe_dict['bin'] = shoe.bin.id
            shoe_list.append(shoe_dict)
        return JsonResponse({'shoes': shoe_list})

    elif request.method == 'POST':
        data = loads(request.body)
        try:
            manufacturer = data['manufacturer']
            model_name = data['model_name']
            color = data['color']
            picture_url = data['picture_url']
            import_href = data['import_href']
            shoe = Shoe(
                manufacturer=manufacturer,
                model_name=model_name,
                color=color,
                picture_url=picture_url,
                import_href=import_href
            )
            shoe.save()
            return JsonResponse({'result': 'success', 'shoe_id': shoe.id}, status=201)
        except KeyError as e:
            return JsonResponse({'result': 'error', 'message': f'Missing field: {str(e)}'}, status=400)
        except Exception as e:
            return JsonResponse({'result': 'error', 'message': str(e)}, status=500)

    elif request.method == 'PUT':
        if shoe_id is None:
            return JsonResponse({'result': 'error', 'message': 'Shoe ID is required for a PUT request'}, status=400)
        data = loads(request.body)
        try:
            shoe = Shoe.objects.get(id=shoe_id)
            shoe.manufacturer = data.get('manufacturer', shoe.manufacturer)
            shoe.model_name = data.get('model_name', shoe.model_name)
            shoe.color = data.get('color', shoe.color)
            shoe.picture_url = data.get('picture_url', shoe.picture_url)
            shoe.import_href = data.get('import_href', shoe.import_href)
            shoe.save()
            return JsonResponse({'result': 'success'}, status=200)
        except Shoe.DoesNotExist:
            return JsonResponse({'result': 'error', 'message': f'Shoe with ID {shoe_id} not found'}, status=404)
        except ValidationError as e:
            return JsonResponse({'result': 'error', 'message': str(e)}, status=400)
        except KeyError as e:
            return JsonResponse({'result': 'error', 'message': f'Missing field: {str(e)}'}, status=400)
        except Exception as e:
            return JsonResponse({'result': 'error', 'message': str(e)}, status=500)

    elif request.method == 'DELETE':
        if shoe_id is None:
            return JsonResponse({'result': 'error', 'message': 'Shoe ID is required for a DELETE request'}, status=400)

        try:
            shoe = Shoe.objects.get(id=shoe_id)
            shoe.delete()
            return JsonResponse({'result': 'success'}, status=200)
        except Shoe.DoesNotExist:
            return JsonResponse({'result': 'error', 'message': f'Shoe with ID {shoe_id} not found'}, status=404)
        except Exception as e:
            return JsonResponse({'result': 'error', 'message': str(e)}, status=500)
