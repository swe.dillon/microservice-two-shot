from django.db import models

class Bin(models.Model):
    id = models.CharField(max_length=50, primary_key=True)
    href = models.CharField(max_length=50)
    closet_name = models.CharField(max_length=50)
    bin_number = models.CharField(max_length=50)
    bin_size = models.CharField(max_length=50)

    def __str__(self):
        return self.id


class Shoe(models.Model):
    manufacturer = models.CharField(max_length=255)
    model_name = models.CharField(max_length=255)
    color = models.CharField(max_length=255)
    picture_url = models.URLField()
    import_href = models.URLField()
    bin = models.ForeignKey(Bin, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return self.manufacturer + ' ' + self.model_name
