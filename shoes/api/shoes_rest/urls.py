from django.urls import path
from . import views

urlpatterns = [
    path('', views.shoe_view, name='shoe_list'),
    path('<int:shoe_id>/', views.shoe_view, name='shoe_detail'),
]
