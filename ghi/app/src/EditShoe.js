import React, { useState, useEffect } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import axios from 'axios';
import { Form, Button } from 'react-bootstrap';

const EditShoe = () => {
  const { id } = useParams();
  console.log('id:', id);
  const navigate = useNavigate();
  const [shoe, setShoe] = useState({});

  useEffect(() => {
    console.log('id:', id);
    const fetchShoe = async () => {
      try {
        if (id) {
          const response = await axios.get(`http://localhost:8080/shoes/${id}`);
          setShoe(response.data.shoe);
        }
      } catch (error) {
        console.error('Error fetching shoe:', error);
      }
    };

    fetchShoe();
  }, [id]);


  const handleChange = (event) => {
    const { name, value } = event.target;
    setShoe({ ...shoe, [name]: value });
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    console.log('handleSubmit called');
    try {
        await axios.put(`http://localhost:8080/shoes/${shoe.id}`, shoe);
      navigate('/shoes');
    } catch (error) {
      console.error('Error updating shoe:', error);
    }
  };



  return (
    <div>
      <h1>Edit Shoe</h1>
      <Form onSubmit={handleSubmit}>
        <Form.Group controlId="manufacturer">
          <Form.Label>Manufacturer</Form.Label>
          <Form.Control
            type="text"
            name="manufacturer"
            value={shoe.manufacturer || ''}
            onChange={handleChange}
          />
        </Form.Group>
        <Form.Group controlId="model_name">
          <Form.Label>Model Name</Form.Label>
          <Form.Control
            type="text"
            name="model_name"
            value={shoe.model_name || ''}
            onChange={handleChange}
          />
        </Form.Group>
        <Form.Group controlId="color">
          <Form.Label>Color</Form.Label>
          <Form.Control
            type="text"
            name="color"
            value={shoe.color || ''}
            onChange={handleChange}
          />
        </Form.Group>
        <Form.Group controlId="picture_url">
          <Form.Label>Picture URL</Form.Label>
          <Form.Control
            type="text"
            name="picture_url"
            value={shoe.picture_url || ''}
            onChange={handleChange}
          />
        </Form.Group>
        <Form.Group controlId="bin">
          <Form.Label>Bin</Form.Label>
          <Form.Control
            type="text"
            name="bin"
            value={shoe.bin || ''}
            onChange={handleChange}
          />
        </Form.Group>
        <Button variant="primary" type="submit">
          Save Changes
        </Button>
      </Form>
    </div>
  );
};

export default EditShoe;
