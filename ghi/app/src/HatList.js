import React, { useEffect, useState } from 'react'

function HatList() {

    const [hats, setHats] = useState([])


    const fetchData = async ()=> {
        const url = 'http://localhost:8090/api/hats/';

        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setHats(data.hats)
            console.log(data)
        }
    }

    useEffect(() => {
        fetchData();
    }, [])

    return (
        <div>
        <table className="table table-striped">
            <thead>
            <tr>
                <th>Name</th>
                <th>Color</th>
                <th>Fabric</th>
                <th>Style</th>
                <th>Picture</th>
                <th>Location</th>
            </tr>
            </thead>
            <tbody>
            {hats.map((hat) => {
            return (
                <tr key={hat.id} value={hat.id}>
                    <td key={hat.name}>{hat.name}</td>
                    <td key={hat.color}>{hat.color}</td>
                    <td key={hat.fabric}>{hat.fabric}</td>
                    <td key={hat.style_name}>{hat.style_name}</td>
                    <td key={hat.picture}>
                        <img src={hat.picture} width="100px" height="100px"/>
                    </td>
                    <td key={hat.location.closet_name}>{hat.location.closet_name}</td>
                </tr>
            );
        })}
            </tbody>
        </table>
        </div>
    )
    }

export default HatList;
