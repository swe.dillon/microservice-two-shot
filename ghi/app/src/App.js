import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoeList from './ShoeList';
import AddShoe from './AddShoe';
import EditShoe from './EditShoe';
import HatList from './HatList';
import HatForm from './HatForm';

function App() {


  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/shoes" element={<ShoeList />} />
          <Route path="/add-shoe" element={<AddShoe />} />
          <Route path="/edit-shoe/:id" element={<EditShoe />} />
          <Route path="hats" >
            {/* <Route path="" element={<HatList />} /> */}
            <Route path="" element={<HatForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
