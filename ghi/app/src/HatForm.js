import React, { useEffect, useState } from "react";

function AttendForm() {
    const [locations, setLocations] = useState([]);

    const [location, setLocation] = useState('');
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }
    const [name, setName] = useState('');
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const [color, setColor] = useState('');
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }
    const [fabric, setFabric] = useState('');
    const handleFabricChange = (event) => {
        const value = event.target.value;
        setFabric(value);
    }
    const [style, setStyle] = useState('');
    const handleStyleChange = (event) => {
        const value = event.target.value;
        setStyle(value);
    }
    const [picture, setPicture] = useState('');
    const handlePictureChange = (event) => {
        const value = event.target.value;
        setPicture(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data ={};

        data.name = name;
        data.color = color;
        data.fabric = fabric;
        data.style_name = style;
        data.picture = picture;
        data.location = location;

        console.log(data);

        const hatsURL = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(hatsURL, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            console.log(newHat)

            setName('');
            setColor('');
            setFabric('');
            setStyle('');
            setPicture('');
            setLocation('');
        }
    }
    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    return (
        <form onSubmit={handleSubmit}>
            <label>
                Name:
                <input type="text" value={name} onChange={handleNameChange} />
            </label>
            <label>
                Color:
                <input type="text" value={color} onChange={handleColorChange} />
            </label>
            <label>
                Fabric:
                <input type="text" value={fabric} onChange={handleFabricChange} />
            </label>
            <label>
                Style:
                <input type="text" value={style} onChange={handleStyleChange} />
            </label>
            <label>
                Picture:
                <input type="text" value={picture} onChange={handlePictureChange} />
            </label>
            <label>
                Location:
                <select value={location} onChange={handleLocationChange}>
                    <option value="">Select a location</option>
                    {locations.map((location) => {
                    return (
                        <option key={location.href} value={location.href}>
                            {location.closet_name}
                        </option>
                    );
                })}
                </select>
            </label>
            <button type="submit">Submit</button>
        </form>
    );
}

export default AttendForm;
