import React, { useState } from 'react';
import axios from 'axios';
import { Container, Form, Button } from 'react-bootstrap';

const AddShoe = () => {
  const [shoe, setShoe] = useState({
    manufacturer: '',
    model_name: '',
    color: '',
    picture_url: '',
    bin: ''
  });

  const handleChange = (event) => {
    setShoe({ ...shoe, [event.target.name]: event.target.value });
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    try {
      await axios.post('http://localhost:8080/shoes', shoe);
      alert('Shoe added successfully');
    } catch (error) {
      console.error('Error adding shoe:', error);
    }
  };
    return (
      <Container>
        <h1>Add Shoe</h1>
        <Form onSubmit={handleSubmit}>
          <Form.Group controlId="manufacturer">
            <Form.Label>Manufacturer</Form.Label>
            <Form.Control
              type="text"
              name="manufacturer"
              value={shoe.manufacturer}
              onChange={handleChange}
            />
          </Form.Group>
          <Form.Group controlId="model_name">
            <Form.Label>Model Name</Form.Label>
            <Form.Control
              type="text"
              name="model_name"
              value={shoe.model_name}
              onChange={handleChange}
            />
          </Form.Group>
          <Form.Group controlId="color">
            <Form.Label>Color</Form.Label>
            <Form.Control
              type="text"
              name="color"
              value={shoe.color}
              onChange={handleChange}
            />
          </Form.Group>
          <Form.Group controlId="picture_url">
            <Form.Label>Picture URL</Form.Label>
            <Form.Control
              type="text"
              name="picture_url"
              value={shoe.picture_url}
              onChange={handleChange}
            />
          </Form.Group>
          <Form.Group controlId="bin">
            <Form.Label>Bin</Form.Label>
            <Form.Control
              type="text"
              name="bin"
              value={shoe.bin}
              onChange={handleChange}
            />
          </Form.Group>
          <Button variant="primary" type="submit">
            Add Shoe
          </Button>
        </Form>
      </Container>
    );
};

export default AddShoe;
