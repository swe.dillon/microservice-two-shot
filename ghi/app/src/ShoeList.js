import React, { useEffect, useState } from 'react';
import axios from 'axios';
import Table from 'react-bootstrap/Table';
import Button from 'react-bootstrap/Button';
import { Link } from 'react-router-dom';

const ShoeList = () => {
  const [shoes, setShoes] = useState([]);
  const [error, setError] = useState(null);

  useEffect(() => {
    const fetchShoes = async () => {
      try {
        const response = await axios.get("http://localhost:8080/shoes");
        setShoes(response.data.shoes);
      } catch (error) {
        setError(error);
      }
    };

    fetchShoes();
  }, []);

  const deleteShoe = async (shoeId) => {
    try {
      await axios.delete(`http://localhost:8080/shoes/${shoeId}`);
      setShoes(shoes.filter((shoe) => shoe.id !== shoeId));
    } catch (error) {
      console.error('Error deleting shoe:', error);
    }
  };

  if (error) {
    return <div>Error: {error.message}</div>;
  }

  return (
    <div>
      <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
        <h1>Shoe List</h1>
        <Link to="/add-shoe">
          <Button variant="primary">Add Shoe</Button>
        </Link>
      </div>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Manufacturer</th>
            <th>Model Name</th>
            <th>Color</th>
            <th>Picture</th>
            <th>Bin</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {shoes.map((shoe) => (
            <tr key={shoe.manufacturer + shoe.model_name}>
              <td>{shoe.manufacturer}</td>
              <td>{shoe.model_name}</td>
              <td>{shoe.color}</td>
              <td>
                <img src={shoe.picture_url} alt={`${shoe.manufacturer} ${shoe.model_name}`} style={{ maxWidth: '100px' }} />
              </td>
              <td>{shoe.bin}</td>
              <td>
                <Link to={`/edit-shoe/${shoe.id}`}>
                  <Button variant="primary" className="me-2">Edit Shoe</Button>
                </Link>
                <Button variant="danger" onClick={() => deleteShoe(shoe.id)}>
                  Delete
                </Button>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </div>
  );
};

export default ShoeList;
